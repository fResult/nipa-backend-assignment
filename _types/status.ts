export enum TicketStatus {
  PENDING = "pending",
  ACCEPTED = "accepted",
  RESOLVED = "resolved",
  REJECTED = "rejected",
}

export enum UserStatus {
  AVAILABLE = "available",
  BANNED = "banned",
}
