import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { LocalStrategy } from "./stategies/local.strategy";

import { UsersModule } from "./../users/users.module";
import { AuthController } from "./controllers/auth/auth.controller";
import { AuthService } from "./service/auth.service";

@Module({
  imports: [UsersModule, PassportModule],
  providers: [AuthService, LocalStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
