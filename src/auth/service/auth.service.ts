import { Injectable } from "@nestjs/common";
import * as argon2 from "argon2";
import { UsersService } from "../../users/services/users.service";

@Injectable()
export class AuthService {
  constructor(
    // @Inject(forwardRef(UsersService))
    private usersService: UsersService,
  ) {}

  async validateUser(email: string, password: string) {
    const user = await this.usersService.findByEmail(email);

    if (argon2.verify(user.password, password)) {
      return user;
    }

    // throw new UnauthorizedException("Email or Password is incorrect");
    return null;
  }
}
