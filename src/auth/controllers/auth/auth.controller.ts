import { ClassSerializerInterceptor, Controller, Post, Request, UseGuards, UseInterceptors } from "@nestjs/common";
import { User } from "../../../users/entities/user.entity";
import { LocalAuthGuard } from "../../guards/local-auth.guard";

@UseInterceptors(ClassSerializerInterceptor)
@Controller("auth")
export class AuthController {
  @UseGuards(LocalAuthGuard)
  @Post("login")
  async login(@Request() req: { user: User }) {
    return req.user;
  }
}
