import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
} from "@nestjs/common";
import { CreateTicketDto } from "../dto/create-ticket.dto";
import { Ticket } from "../entities/ticket.entity";
import { TicketsService } from "../services/tickets.service";
import { UpdateTicketDto } from "./../dto/update-ticket.dto";

type QueryParams = UpdateTicketDto;

@UseInterceptors(ClassSerializerInterceptor)
@Controller("tickets")
export class TicketsController {
  constructor(private ticketsService: TicketsService) {}

  @Get("/")
  async findAll(@Query() queryParams: QueryParams): Promise<{ data: Ticket[] }> {
    return { data: await this.ticketsService.findAll(queryParams) };
  }

  @Get("/:id")
  async findOne(@Param("id") id: string): Promise<{ data: Ticket }> {
    return { data: await this.ticketsService.findOne(id) };
  }

  @Post("/")
  async create(@Body() body: CreateTicketDto): Promise<{ data: Ticket }> {
    return { data: await this.ticketsService.create(body) };
  }

  @Patch("/:id")
  async update(@Param("id") id: string, @Body() body: UpdateTicketDto): Promise<{ data: Ticket }> {
    return { data: await this.ticketsService.update(id, body) };
  }
}
