import { Type } from "class-transformer";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { TicketStatus } from "../../../_types/status";
import { User } from "../../users/entities/user.entity";

@Entity("tickets")
export class Ticket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  subject: string;

  @Column({ nullable: true })
  description: string;

  @Column({ default: TicketStatus.PENDING })
  status: TicketStatus;

  @ManyToOne<User>(() => User, (user) => user.tickets)
  @JoinColumn({ name: "user_id" })
  @Type(() => User)
  user: User;

  @CreateDateColumn({ name: "created_at", default: new Date() })
  createdAt: Date;

  @UpdateDateColumn({ name: "updated_at", default: new Date() })
  updatedAt: Date;
}
