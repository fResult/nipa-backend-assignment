import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Ticket } from "../entities/ticket.entity";
import { CreateTicketDto } from "../dto/create-ticket.dto";
import { UpdateTicketDto } from "../dto/update-ticket.dto";
import { User } from "../../users/entities/user.entity";

@Injectable()
export class TicketsService {
  constructor(
    @InjectRepository(Ticket)
    private ticketsRepository: Repository<Ticket>,
  ) {}

  async findAll(queryParams: Partial<Ticket>): Promise<Ticket[]> {
    return await this.ticketsRepository.find({
      relations: [User.name.toLowerCase()],
      where: { ...queryParams },
    });
  }

  async findOne(id: string): Promise<Ticket> {
    const ticket = await this.ticketsRepository.findOne(id, {
      relations: [User.name.toLowerCase()],
    });
    if (!ticket) {
      throw new NotFoundException(`Ticket ID #${id} not found`);
    }
    return ticket;
  }

  async create(createTicketDto: CreateTicketDto): Promise<Ticket> {
    return await this.ticketsRepository.save(createTicketDto);
  }

  async update(id: string, updateTicketDto: UpdateTicketDto): Promise<Ticket> {
    const foundTicket = await this.findOne(id);

    const updatedTicket: Ticket = {
      ...foundTicket,
      ...updateTicketDto,
    };
    return this.ticketsRepository.save(updatedTicket);
  }
}
