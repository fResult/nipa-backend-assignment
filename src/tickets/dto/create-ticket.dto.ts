import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsEnum, IsNumber, IsOptional, IsString, Length } from "class-validator";

import { User } from "../../users/entities/user.entity";
import { TicketStatus } from "../../../_types/status";

export class CreateTicketDto {
  @IsString()
  @Length(5, 80)
  @ApiProperty()
  readonly subject: string;

  @IsString()
  @Length(5, 255)
  @IsOptional()
  @ApiProperty()
  readonly description: string;

  @IsOptional()
  @IsEnum(TicketStatus)
  @ApiProperty()
  readonly status: TicketStatus;

  @IsNumber()
  @ApiProperty({ description: "Customer who create this ticket" })
  readonly user: User;

  @IsOptional()
  @IsDate()
  @ApiProperty()
  readonly createdAt: Date;

  @IsOptional()
  @IsDate()
  @ApiProperty()
  readonly updatedAt: Date;
}
