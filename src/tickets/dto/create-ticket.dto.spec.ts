import { CreateTicketDto } from "./create-ticket.dto";

describe("CreateTicketDto", () => {
  it("should be defined", () => {
    expect(new CreateTicketDto()).toBeDefined();
  });
});
