import { UpdateTicketDto } from "./update-ticket.dto";

describe("UpdateTicketDto", () => {
  it("should be defined", () => {
    expect(new UpdateTicketDto()).toBeDefined();
  });
});
