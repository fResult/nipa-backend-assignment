import { TicketsService } from "./services/tickets.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from "@nestjs/common";
import { Ticket } from "./entities/ticket.entity";
import { TicketsController } from "./controllers/tickets.controller";

@Module({
  imports: [TypeOrmModule.forFeature([Ticket])],
  controllers: [TicketsController],
  providers: [TicketsService],
})
export class TicketsModule {}
