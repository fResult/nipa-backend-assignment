import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import * as joi from "joi";
import { Env } from "../_types/environment";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Connection } from "typeorm";
import { UsersModule } from "./users/users.module";
import { TicketsModule } from "./tickets/tickets.module";
import { AuthModule } from "./auth/auth.module";
import configuration from "../configs/configuration";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `${process.cwd()}/configs/.env.${process.env.NODE_ENV || "development"}`,
      load: [configuration],
      validationSchema: joi.object({
        port: joi.number().default(8080),
        apiKey: joi.string(),
        database: joi.object({
          database: joi.string(),
          host: joi.string(),
          port: joi.number().default(5432),
          username: joi.string(),
          password: joi.string(),
        }),
      }),
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async () => {
        return {
          type: "postgres",
          host: process.env.DATABASE_HOST,
          port: +process.env.DATABASE_PORT || 5432,
          database: process.env.DATABASE_NAME,
          username: process.env.DATABASE_USER,
          password: process.env.DATABASE_PASSWORD,
          logging: ["query", "error"],
          autoLoadEntities: true,
          synchronize: process.env.NODE_ENV === Env.DEVELOPMENT,
        };
      },
    }),
    UsersModule,
    TicketsModule,
    AuthModule,
    // TypeOrmModule.forRoot({
    //   type: "postgres",
    //   host: process.env.DATABASE_HOST,
    //   port: +process.env.DATABASE_PORT || 5432,
    //   database: process.env.DATABASE_NAME,
    //   username: process.env.DATABASE_USER,
    //   password: process.env.DATABASE_PASSWORD,
    //   autoLoadEntities: true,
    //   logging: ["query", "error"],
    //   synchronize: process.env.NODE_ENV === Env.DEVELOPMENT,
    // }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
