import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsMobilePhone, IsNotEmpty, IsString, Length, IsOptional, IsEnum } from "class-validator";
import { Role } from "_types/role";
import { UserStatus } from "../../../_types/status";

export class CreateUserDto {
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @IsString()
  @Length(8, 32)
  @ApiProperty({ minLength: 8, maxLength: 32 })
  readonly password: string;

  @IsString()
  @Length(2, 50)
  @ApiProperty({ minLength: 2, maxLength: 50 })
  readonly firstName: string;

  @IsString()
  @Length(3, 70)
  @ApiProperty({ minLength: 3, maxLength: 70 })
  readonly lastName: string;

  @IsMobilePhone("th-TH")
  @IsNotEmpty()
  @ApiProperty()
  readonly phoneNo: string;

  @IsOptional()
  @IsEnum(Role)
  @ApiProperty({ enum: Role })
  readonly role: Role;

  @IsString()
  @IsOptional()
  @ApiProperty({ enum: UserStatus })
  readonly status: UserStatus;
}
