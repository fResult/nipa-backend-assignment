import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from "@nestjs/common";
import { CreateUserDto } from "../dto/create-user.dto";
import { UpdateUserDto } from "../dto/update-user.dto";
import { User } from "../entities/user.entity";
import { UsersService } from "../services/users.service";

@UseInterceptors(ClassSerializerInterceptor)
@Controller("users")
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get("/")
  async findAll(): Promise<{ data: User[] }> {
    return { data: await this.userService.findAll() };
  }

  @Get("/:id")
  async findOne(@Param("id") id: string): Promise<{ data: User }> {
    return { data: await this.userService.findOne(id) };
  }

  @Post("/")
  async create(@Body() body: CreateUserDto): Promise<{ data: CreateUserDto }> {
    return { data: await this.userService.create(body) };
  }

  @Patch(":id")
  async update(@Param("id") id: string, @Body() body: UpdateUserDto): Promise<{ data: UpdateUserDto }> {
    return { data: await this.userService.update(id, body) };
  }

  @Delete("/:id")
  async delete(@Param("id") id: string): Promise<{ data: User }> {
    return { data: await this.userService.remove(id) };
  }
}
