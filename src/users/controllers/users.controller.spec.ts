import { Test, TestingModule } from "@nestjs/testing";
import { UsersService } from "../services/users.service";
import { UsersController } from "./users.controller";

describe("UsersController", () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    // service = module.get<UsersService>(UsersService);
    service = new UsersService();
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });

  it("findAll", async () => {
    const expectedUsers = [
      { id: 1, name: "user_name_1" },
      { id: 2, name: "user_name_2" },
    ];
    jest.spyOn(service, "findAll").mockImplementation(() => [expectedUsers[1]]);

    const actual = await controller.findAll();

    expect(actual.data.length).toBeGreaterThanOrEqual(0);
    expect(actual.data[0]?.name).toEqual(expectedUsers[0]?.name);

    expect(actual).toEqual({ data: expectedUsers });
  });
});
