import { Injectable, NotFoundException, UnprocessableEntityException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as argon2 from "argon2";

import { CreateUserDto } from "../dto/create-user.dto";
import { UpdateUserDto } from "../dto/update-user.dto";
import { User } from "../entities/user.entity";

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepository: Repository<User>) {}

  async findAll(param?: Partial<User>): Promise<User[]> {
    return await this.userRepository.find({ where: { ...param } });
  }

  async findOne(id: string): Promise<User> {
    const user = await this.userRepository.findOne(id);
    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    return user;
  }

  async findByEmail(email: string): Promise<User> {
    const user = this.userRepository.findOne({ where: { email } });
    if (!user) {
      throw new NotFoundException(`User email: ${email} not found`);
    }

    return user;
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const oldUser = await this.findByEmail(createUserDto.email);
    if (oldUser) {
      throw new UnprocessableEntityException(`Email: ${createUserDto.email} already exists`);
    }

    const newUser = this.userRepository.create({
      ...createUserDto,
      password: await argon2.hash(createUserDto.password),
    });
    return await this.userRepository.save(newUser);
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const foundUser = await this.userRepository.findOne(id);

    // TODO: Consider to use preload function, when Ticket Entity and API is ready.
    // const updatedUser = await this.userRepository.preload({ ...foundUser, ...updateUserDto });
    if (/*!updatedUser &&*/ !foundUser) {
      throw new NotFoundException(`User ID #${id} not found`);
    }

    if (updateUserDto.password) {
      updateUserDto = {
        ...updateUserDto,
        password: await argon2.hash(updateUserDto.password),
      };
    }

    return await this.userRepository.save({ ...foundUser, ...updateUserDto });
  }

  async remove(id: string) {
    const user = await this.findOne(id);
    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    this.userRepository.delete(id);
    return user;
  }
}
