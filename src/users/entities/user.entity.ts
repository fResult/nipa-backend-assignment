import { Exclude } from "class-transformer";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Role } from "../../../_types/role";
import { UserStatus } from "../../../_types/status";
import { Ticket } from "../../tickets/entities/ticket.entity";

@Unique(["email"])
@Entity("users")
export class User {
  @PrimaryGeneratedColumn()
  id: string;

  @Exclude()
  @Column()
  password: string;

  @Column({ name: "first_name" })
  firstName: string;

  @Column({ name: "last_name" })
  lastName: string;

  @Column({ name: "phone_no" })
  phoneNo: string;

  @Column({ unique: true })
  email: string;

  @Column({ default: Role.CUSTOMER })
  role: Role;

  @Column({ default: UserStatus.AVAILABLE })
  status: UserStatus;

  @OneToMany<Ticket>(() => Ticket, (ticket) => ticket.user)
  tickets: Ticket[];
}
