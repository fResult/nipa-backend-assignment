import { ConfigFactory } from "@nestjs/config";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export interface IAppConfig {
  port: number;
  apiKey: string;
  database: TypeOrmModuleOptions;
}

const configuration: ConfigFactory<IAppConfig> = () => {
  return {
    port: +process.env.PORT,
    apiKey: process.env.API_KEY,
    database: {
      database: process.env.DATABASE_NAME,
      host: process.env.DATABASE_HOST,
      port: +process.env.DATABASE_PORT,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
    },
  };
};

export default configuration;
